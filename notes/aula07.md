#Aula 07 - 21/09

# Arquitetura de Banco de Dados

```
                    Transações externas
.-----------------------------+----------------------------.
|                             v                            |
|                     .----------------.                   |
|                     | Gerenciador de |                   |
|                     |   Transações   |     SGBD          |
|                     `----------------´                   |
|                             ^                            |
|                             |                            |
|                             v                            |
|                      .-------------.                     |
|                      | Escalonador |                     |
|                      `-------------´                     |
|                             ^                            |
| .---------------------------+-------------------------.  |
| |                           v                         |  |
| |                  .----------------.                 |  |
| |                  | Gerenciador de |                 |  |
| |                  |   Recuperação  |                 |  |
| |    .----.        `----------------´                 |  |
| |    | BD |<---.            ^                         |  |
| |    `----´     \           |                         |  |
| |                \          v                         |  |
| | .-------.       \ .----------------.    .-------.   |  |
| | | Index |<--------| Gerenciador de |<-->| Cache |   |  |
| | `-------´       / |   Memória      |    `-------´   |  |
| |                /  `----------------´                |  |
| |   .-----.     /                                     |  |
| |   | Log |<---´                                      |  |
| |   `-----´                                           |  |
| |                                                     |  |
| |   Memória                               Memória     |  |
| |   Secundária                            Primária    |  |
| `-----------------------------------------------------´  |
|                                                          |
|                                                          |
`----------------------------------------------------------´
```

# Indexação

Se o dado é estruturado então o dominío é estruturado e discreto. Abaixo temos o exemplo
de um arquivo de indexação:

| NUSP\_aluno | Conteúdo |
|-------------|----------|
| 8222222     | 002      |
| 8222226     | 063      |
| 8222300     | 108      |
| 8224202     | 958      |

A coluna *conteúdo* aponta onde está escrito o arquivo com o resto dos dados dos alunos.

Podemos usar várias estruturas de para interpretar esses arquivos, como um vetor
ordenado com busca binária, uma árvore binária ou árvore B.

Na árvore binária, a complexidade de busca é $`log_2 n`$, porém na árvore B, a
complexidade é $`log_k n`$, onde k é maior que 2.

Toda vez que criamos uma **primary key** no SQL, ele já cria os arquivos de índice para
essas chaves.

**Nota**: em SGBDs baseados em grafos, não temos índices, pois a busca nesses BDs são
feitas através de navegação em grafos.

## Árvore B+
Nas folhas internas temos particionadores de intervalos para facilitar a busca, todo
o conteúdo está nas folhas. Na árvore B normal, o conteúdo fica nos nós internos também,
além

```
         .-+-------+-+----+------+-+----+-.
         | |   10  | | 20 | | 30 | | 40 | |
         `-+-------+-+----+------+-+----+-´
          |         |      |      |      |
          |         |      |      |      v
          |         |      |      | .--------------.
          |         |      |      | | 42 | 35 | 48 |
          |         |      |      | `--------------´
          |         |      |      |   ^
          |         |      |      v   |
          |         |      | .--------------.
          |         |      | | 32 | 34 | 36 |
          |         |      | `--------------´
          |         |      |   ^
          |         |      v   |
          |         | .--------------.
          |         | | 21 | 26 | 29 |
          |         | `--------------´
          |         |   ^
          |         v   |
          | .--------------.
          | | 13 | 15 | 19 |
          | `--------------´
          |   ^
          v   |
   .---------------.
   | 1 | 2 | 4 | 7 |
   `---------------´
```
