# Aula 01

## Avaliações
P1 -> 21/09
P2 -> 09/11

MF = (2P1 + 3P2)/5

### Conteúdo
* Estrutura de Dados: BD orientado a grafo e outros NoSQLs

#### Indexação
* **Tradicional**: árvore binária e recuperação de dados
  * Dados estruturados: domínio discreto e bem definido
  * Dados não estruturados: imagens e texto
* **Hashing**

#### Transações
* **Clássicas**: ACID, transações online, que são feitas em milisegundos

* **Não Clássicas**: transações longas
  * workflows
  * paralelismo

* **Sistemas Cientes de Processos**
  * work (atividade)
  * eventos (tempo, dado): ECA
  * dados

* **Integração de work, event e dataflow**

```
    Y (MapReduce)
    ^
   P|
   a|     /------------------/
   r|    /                  /
   a|   /                  /
   l|  |      dinâmica    |
   e|  |                  |
   l|  |                  |
   s|  |------------------|
   m|
   o|-----------------------> X (work)
            Seriação
```
