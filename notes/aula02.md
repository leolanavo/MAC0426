# Aula 02

## Banco de Dados orientados em Grafos
```
    .--------. M
    | Pessoa |---.
    '--------'   |
        |N       |
        |        |
        '--------'
```

Grau de indexação de relacionamento alto

```
    .------------------------.
    | Id Origem | Id Destino |
    |-----------|------------|
    |     1     |      2     | <-----|
    |-----------+------------|       |
    |     1     |      3     |       |
    |-----------+------------|       |
    |     2     |      4     | <-----|- (1)
    |-----------+------------|       |
    |     2     |      1     |       |
    |-----------+------------|       |
    |     4     |      4     | <-----|
    '------------------------'
```

(1) Limite da solução relacional

### Arquitetura no mundo real
```
    .------------------------.  dado replicado
    |               |xxxxxxxx|------------------> Grafo
    |               '--------|
    |       Relacional       |
    |                        |
    |                        |
    '------------------------'

```

### Representação de relações usando grafos
```
    .-------------.   FRIEND_OF   .-------------.
    | user: Alice |<------------->| user: James |
    '-------------'               '-------------'
           ^                             |
           |                             |
           |                             | BOSS_OF
           |                             |
           |                             |
           |                             v
           |  COLLEAGE_OF         .-------------.
           '--------------------->| user: Clark |
                                  '-------------'
```

As entidades são relacionadas fisicamente com outras entidades, as relações se
dão pelas arestas, e seus tipos.
