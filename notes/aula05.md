# Aula 05

A busca por adjacências em grafos é livre de indíces.

Relacionamentos derivados têm seu foco em fazer recomendações para o usuário.

```
               |
               |
    Key-Value  |  Graph
               |
               |
   ------------+-------------
               |
               |
    Column     |  Document
    Family     |
    (Bitmap)   |
               |
```
